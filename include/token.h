#ifndef TOKEN_H
#define TOKEN_H

#include <string>
#include <tokenType.h>

class Token
{
public:
    TokenType type;
    std::string lexeme;
    int line;

    Token(TokenType type, std::string lexeme, int line):
        type(type), lexeme(lexeme), line(line){}
};

std::ostream& operator<<(std::ostream &stream, Token token)
{
    return stream << "[line " << token.line << "] " << token.type << " " << token.lexeme;
}

#endif
