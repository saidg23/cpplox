#ifndef SCANNER_H
#define SCANNER_H

#include <string>
#include <list>
#include <token.h>
#include <unordered_map>

class Scanner
{
public:
    std::string source;
    std::list<Token> tokens;
    static void (*error)(int, std::string);
    size_t start = 0;
    size_t current = 0;
    size_t line = 1;
    static inline std::unordered_map<std::string, TokenType> keywords;

    Scanner(std::string &source): source(source) {}

    std::list<Token> scanTokens()
    {
        while(!isAtEnd())
        {
            start = current;
            scanToken();
        }

        tokens.push_back(Token(LEOF, "", line));
        return tokens;
    }

    void scanToken()
    {
        char c = advance();
        switch(c)
        {
            case '(': addToken(LEFT_PAREN); break;
            case ')': addToken(RIGHT_PAREN); break;
            case '{': addToken(LEFT_BRACE); break;
            case '}': addToken(RIGHT_BRACE); break;
            case ',': addToken(COMMA); break;
            case '.': addToken(DOT); break;
            case '-': addToken(MINUS); break;
            case '+': addToken(PLUS); break;
            case ';': addToken(SEMICOLON); break;
            case '*': addToken(STAR); break;
            case '!': addToken(match('=') ? BANG_EQUAL : BANG); break;
            case '=': addToken(match('=') ? EQUAL_EQUAL : EQUAL); break;
            case '<': addToken(match('=') ? LESS_EQUAL : LESS); break;
            case '>': addToken(match('=') ? GREATER_EQUAL : GREATER); break;
            case '/':
                if(match('/'))
                {
                    while(peek() != '\n' && !isAtEnd()) advance();
                }
                else
                {
                    addToken(SLASH);
                }
                break;
            case ' ':
            case '\r':
            case '\t':
                break;
            case '\n':
                line++;
                break;
            case '"': string(); break;
            default:
                if(isDigit(c))
                {
                    number();
                }
                else if(isAlpha(c))
                {
                    identifier();
                }
                else
                {
                    error(line, "Unexpected character.");
                }
                break;
        }
    }

    void identifier()
    {
        while(isAlphaNumeric(peek())) advance();

        std::string text = source.substr(start, current - start);
        TokenType type;
        if(!(keywords.find(text) == keywords.end())) type = keywords[text];
        else type = IDENTIFIER;
        addToken(type);
    }

    void number()
    {
        while(isDigit(peek())) advance();

        if(peek() == '.' && isDigit(peekNext()))
        {
            advance();
        }

        while(isDigit(peek())) advance();

        addToken(NUMBER);
    }

    void string()
    {
        while(peek() != '"' && !isAtEnd())
        {
            if(peek() == '\n') line++;
            advance();
        }

        if(isAtEnd())
        {
            error(line, "Untermintated string.");
            return;
        }

        advance();

        addToken(STRING);
    }

    bool match(char expected)
    {
        if(isAtEnd()) return false;
        if(source[current] != expected) return false;

        current++;
        return true;
    }

    char peek()
    {
        if(isAtEnd()) return '\0';
        return source[current];
    }

    char peekNext()
    {
        if(current + 1 >= source.length()) return '\0';
        return source[current + 1];
    }

    bool isAlphaNumeric(char c)
    {
        return isAlpha(c) || isDigit(c);
    }

    bool isAlpha(char c)
    {
        return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_';
    }

    bool isDigit(char c)
    {
        return c >= '0' && c <= '9';
    }

    bool isAtEnd()
    {
        return current >= source.length() - 1;
    }

    char advance()
    {
        return source[current++];
    }

    void addToken(TokenType type)
    {
        std::string text = source.substr(start, current - start);
        tokens.push_back(Token(type, text, line));
    }

    static void init()
    {
        keywords = std::unordered_map<std::string, TokenType>();
        keywords.insert({"and", AND});
        keywords.insert({"class", CLASS});
        keywords.insert({"else", ELSE});
        keywords.insert({"false", FALSE});
        keywords.insert({"fun", FUN});
        keywords.insert({"for", FOR});
        keywords.insert({"if", IF});
        keywords.insert({"nil", NIL});
        keywords.insert({"or", OR});
        keywords.insert({"print", PRINT});
        keywords.insert({"return", RETURN});
        keywords.insert({"super", SUPER});
        keywords.insert({"this", THIS});
        keywords.insert({"true", TRUE});
        keywords.insert({"var", VAR});
        keywords.insert({"while", WHILE});
    }
};

namespace Lox
{
    void error(int line, std::string message);
}

void (*Scanner::error)(int, std::string) = Lox::error;

#endif
