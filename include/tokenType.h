#ifndef TOKEN_TYPE
#define TOKEN_TYPE

#include <iostream>

enum TokenType {
    // Single-character tokens.
    LEFT_PAREN, RIGHT_PAREN, LEFT_BRACE, RIGHT_BRACE,
    COMMA, DOT, MINUS, PLUS, SEMICOLON, SLASH, STAR,

    // One or two character tokens.
    BANG, BANG_EQUAL,
    EQUAL, EQUAL_EQUAL,
    GREATER, GREATER_EQUAL,
    LESS, LESS_EQUAL,

    // Literals.
    IDENTIFIER, STRING, NUMBER,

    // Keywords.
    AND, CLASS, ELSE, FALSE, FUN, FOR, IF, NIL, OR,
    PRINT, RETURN, SUPER, THIS, TRUE, VAR, WHILE,

    LEOF
};

std::ostream& operator<< (std::ostream &stream, TokenType token)
{
    switch(token)
    {
        case LEFT_PAREN: return stream << "LEFT_PAREN";
        case RIGHT_PAREN: return stream << "RIGHT_PAREN";
        case LEFT_BRACE: return stream << "LEFT_BRACE";
        case RIGHT_BRACE: return stream << "RIGHT_BRACE";
        case COMMA: return stream << "COMMA";
        case DOT: return stream << "DOT";
        case MINUS: return stream << "MINUS";
        case PLUS: return stream << "PLUS";
        case SEMICOLON: return stream << "SEMICOLON";
        case SLASH: return stream << "SLASH";
        case STAR: return stream << "STAR";
        case BANG: return stream << "BANG";
        case BANG_EQUAL: return stream << "BANG_EQUAL";
        case EQUAL: return stream << "EQUAL";
        case EQUAL_EQUAL: return stream << "EQUAL_EQUAL";
        case GREATER: return stream << "GREATER";
        case GREATER_EQUAL: return stream << "GREATER_EQUAL";
        case LESS: return stream << "LESS";
        case LESS_EQUAL: return stream << "LESS_EQUAL";
        case IDENTIFIER: return stream << "IDENTIFIER";
        case STRING: return stream << "STRING";
        case NUMBER: return stream << "NUMBER";
        case AND: return stream << "AND";
        case CLASS: return stream << "CLASS";
        case ELSE: return stream << "ELSE";
        case FALSE: return stream << "FALSE";
        case FUN: return stream << "FUN";
        case FOR: return stream << "FOR";
        case IF: return stream << "IF";
        case NIL: return stream << "NIL";
        case OR: return stream << "OR";
        case PRINT: return stream << "PRINT";
        case RETURN: return stream << "RETURN";
        case SUPER: return stream << "SUPER";
        case THIS: return stream << "THIS";
        case TRUE: return stream << "TRUE";
        case VAR: return stream << "VAR";
        case WHILE: return stream << "WHILE";
        default: return stream << "EOF";
    }
}

#endif
