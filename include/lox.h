#ifndef LOX_H
#define LOX_H

#include <list>
#include <scanner.h>

namespace Lox
{
    bool hadError = false;

    void report(int line, std::string where, std::string message)
    {
        std::cerr << "[line " << line << "] Error" << where << ": " << message << std::endl;
        hadError = true;
    }

    void error(int line, std::string message)
    {
        report(line, "", message);
    }


    void run(std::string &source)
    {
        Scanner scanner = Scanner(source);
        std::list tokens = scanner.scanTokens();

        for(auto token: tokens)
        {
            if(token.type == LEOF) break;
            std::cout << token << std::endl;
        }
    }

    void runFile(char *filename)
    {
        std::ifstream script;
        script.open(filename);
        if(script.is_open())
        {
            std::string line;
            std::string source;
            while(std::getline(script, line))
            {
            source += line + '\n';
            }
            script.close();
            run(source);
            if(hadError) exit(EX_DATAERR);
        }
        else
        {
            std::cerr << "failed to load file" << std::endl;
            exit(EX_NOINPUT);
        }
    }

    void runPrompt()
    {
        std::string source;

        while(true)
        {
            std::cout << "> ";
            if(std::getline(std::cin, source).eof() || source == "exit") break;
            run(source += '\n');
            hadError = false;
        }
    }
};

#endif
