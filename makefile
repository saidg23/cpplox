CC = gcc
CXX = g++
CFLAGS = -Wall
CXXFLAGS = -std=c++17 -Wall
INC = -Iinclude
VPATH = src
OBJ = $(patsubst src/%.cpp,obj/%.o,$(wildcard src/*.cpp))

all: lox

lox: $(OBJ)
	$(CXX) -o $@ $(OBJ)

obj/%.o: %.cpp obj
	$(CXX) $(CXXFLAGS) $(INC) -c -o $@ $<

obj:
	mkdir $@

.PHONY: clean

clean:
	rm -r lox obj
