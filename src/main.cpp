#include <iostream>
#include <string>
#include <fstream>
#include <sysexits.h>
#include <lox.h>

int main(int argc, char *argv[])
{
    Scanner::init();

    if(argc > 2)
    {
        std::cout << "Usage: cpplox [script]" << std::endl;
        exit(EX_USAGE);
    }
    else if(argc == 2)
    {
        Lox::runFile(argv[1]);
    }
    else
    {
        Lox::runPrompt();
    }

    return 0;
}
